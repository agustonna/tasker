require 'bundler'
require 'date'
require 'dotenv'

Dotenv.load
Bundler.require

require "./basecamp_client"

class Tasker
  PROCESSED_TODO_IDS = "processed_todo_ids" #redis set key name
  def initialize(username, password)
    uri = URI.parse(ENV["REDISTOGO_URL"])
    @client = BasecampClient.new(username, password)
    @redis = Redis.new(:host => uri.host, :port => uri.port, :password => uri.password)
  end

  def run
    @client.projects.each do |project|
      @client.todolists(project["id"]).each do |todolist|
        @client.todolist(project["id"], todolist["id"])["todos"]["completed"].each do |todo|
          process_todo(todo, project, todolist)
        end
      end
    end
  end

  def run_todo_reminder
    @client.projects.each do |project|
      @client.todolists(project["id"]).each do |todolist|
        @client.todolist(project["id"], todolist["id"])["todos"]["remaining"].each do |todo|
          comment_due_todo(todo, project) if todo["due_at"]
        end
      end
    end
  end

  private

  #metodo que agrega al set de redis los ids de los todos que se crean
  def add_todo_id_to_redis(todo)
    @redis.sadd(PROCESSED_TODO_IDS, todo["id"])
  end

  def todo_is_elegible?(todo) # si aca al if le agrego que el due_date = nil entonces sale del metodo y no crea nada
    todo["content"].match(/\[(monthly|weekly|daily)\]/) && !@redis.sismember(PROCESSED_TODO_IDS, todo["id"])
  end

  #metodo para fijarse si los todos completados son recurrentes 
  def process_todo(todo, project, todolist)
    if todo_is_elegible?(todo)
      matched_todo = {}
      matched_todo[:content] = todo["content"]
      if todo["assignee"]
        matched_todo[:assignee] = {}
        matched_todo[:assignee][:id] = todo["assignee"]["id"] 
        matched_todo[:assignee][:type] = todo["assignee"]["type"]
      end
      matched_todo[:due_at] = new_date(todo)
      if @client.create_todo(project["id"], todolist["id"], matched_todo)
        add_todo_id_to_redis(todo)
      end
    end
  end

  #metodo que agrega dia, semana o mes dependiendo de la recurrencia del todo
  def new_date(todo)
    return nil if todo["due_at"].nil?
    
    days_to_increment = case todo["content"]
    when /\[daily\]/
      1
    when /\[weekly\]/
      7
    when /\[monthly\]/
      30
    end

    (Date.parse(todo["due_at"]) + days_to_increment).to_s
  end

  def comment_due_todo(todo, project)
    if (todo["due_at"] == Date.today.to_s) && (todo["completed"] == false)
      comment_content = {}
      comment_content[:content] = "Ojo que vence hoy! Si necesitas ayuda, hacé un comentario mencionando a quien te pueda ayudar. "
      comment_content[:subscribers] = [todo["assignee"]["id"].to_i]
      @client.comment_todo(project["id"], todo["id"], comment_content)
    end
  end
end