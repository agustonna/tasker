require 'bundler'
require 'dotenv'

Dotenv.load
Bundler.require

class BasecampClient
  include HTTParty
  APPLICATION_NAME = "Dreepi (info@dreepi.com)"
  base_uri "https://basecamp.com/#{ENV['BASECAMP_ACCOUNT_ID']}/api/v1/"

  def initialize(username, password)
    @default_options = { :basic_auth => {:username => username, :password => password}, 
                         :headers => {"User-Agent" => APPLICATION_NAME, 'Content-Type' => 'application/json'} }
  end

  def projects(options={})
    opts = options.merge(@default_options)
    self.class.get('/projects.json', opts)
  end

  def todolists(project_id, options={})
    opts = options.merge(@default_options)
    self.class.get("/projects/#{project_id}/todolists.json", opts)
  end

  def todolist(project_id, todolist_id, options={})
    opts = options.merge(@default_options)
    self.class.get("/projects/#{project_id}/todolists/#{todolist_id}.json", opts)
  end

  def create_todo(project_id, todolist_id, todo_info, options={})
    opts = options.merge(@default_options)
    opts[:body] = todo_info.to_json
    self.class.post("/projects/#{project_id}/todolists/#{todolist_id}/todos.json", opts)
  end

  def comment_todo(project_id, todo_id, comment_content, options={})
    opts = options.merge(@default_options)
    opts[:body] = comment_content.to_json
    self.class.post("/projects/#{project_id}/todos/#{todo_id}/comments.json", opts)
  end
end